
exports.up = function(knex) {
    return knex.schema.createTable('genres_films', table => {
        table.increments('id').primary()
        table.integer('genre').unsigned().references('id')
            .inTable('genres').notNull()
        table.integer('film').unsigned().references('id')
            .inTable('films').notNull()
    })  
};

exports.down = function(knex) {
    return knex.schema.dropTable('genres_films')
};
