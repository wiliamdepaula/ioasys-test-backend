
exports.up = function(knex) {
    return knex.schema.createTable('votes', table => {
        table.integer('film').unsigned()
            .references('id').inTable('films').notNull()
        table.integer('person').unsigned()
            .references('id').inTable('persons').notNull()
        table.integer('vote').unsigned().notNull()
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('votes')
};
