
exports.up = function(knex) {
    return knex.schema.createTable('actors', table => {
        table.integer('film').unsigned()
            .references('id').inTable('films').notNull()
        table.integer('person').unsigned()
            .references('id').inTable('persons').notNull()
    })  
};

exports.down = function(knex) {
    return knex.schema.dropTable('actors')
};
