
exports.up = function(knex) {
    return knex.schema.createTable('films', table => {
        table.increments('id').primary()
        table.string('name').notNull()
        table.integer('year').notNull()
        table.string('imageUrl', 1000)
        table.text('description').notNull()
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('films')
};
