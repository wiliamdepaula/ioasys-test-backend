const admin = require('./admin')

module.exports = app => {

    app.post('/signup', app.api.user.save)
    app.post('/signin', app.api.auth.signin)
    app.post('/validateToken', app.api.auth.validateToken)


    app.route('/users')
        .all(app.config.passport.authenticate())
        .post(admin(app.api.user.save))
        .get(admin(app.api.user.get))
    
    app.route('/users/:id')
        .all(app.config.passport.authenticate())
        .get(admin(app.api.user.getById))
        .put(admin(app.api.user.save))
        .delete(admin(app.api.user.remove))

    app.route('/genres')
        .post(app.config.passport.authenticate())
        .post(admin(app.api.genre.save))
        .get(app.api.genre.get)

    app.route('/genres/:id')
        .get(app.api.genre.getById)

    app.route('/films')
        .get(app.api.film.get)
        .post(app.config.passport.authenticate())
        .post(admin(app.api.film.save))

    app.route('/films/:id')
        .get(app.api.film.getById)

    app.route('/films/:id/vote')
        .post(app.config.passport.authenticate())
        .post(app.api.film.vote)

    app.route('/directors')
        .get(app.api.director.get)
       

    app.route('/directors/:id') 
        .get(app.api.director.getById)

    app.route('/actors')
        .get(app.api.actor.get)

    app.route('/actors/:id') 
        .get(app.api.actor.getById)

    app.route('/persons')
        .get(app.api.person.get)
        .post(app.config.passport.authenticate())
        .post(admin(app.api.person.save))


    app.route('/persons/:id') 
        .get(app.api.person.getById)
}