const prompt = require('prompt');
const config = require('../knexfile.js')
const knex = require('knex')(config)
const bcrypt = require('bcrypt-nodejs')

knex.migrate.latest([config])

const encryptPassword = password => {
    const salt = bcrypt.genSaltSync(10)
    return bcrypt.hashSync(password, salt)
}

const properties = [
    {
        name: 'name',
        required: true,
        warning: 'nome não pode ser vazio'
    },
    {
        name: 'email',
        required: true,
        warning: 'email não pode ser vazio'
    },
    {
        name: 'password',
        required: true,
        hidden: true,
        warning: 'senha não pode ser vazia'
    },
    {
        name: 'confirmPassword',
        required: true,
        hidden: true,
        warning: 'confirmação de senha não pode ser vazia'
    }
];

prompt.start();

prompt.get(properties, function (err, result) {
    if (err) { return onErr(err); }
    // console.log('Command-line input received:');
    // console.log('  Username: ' + result.name);
    // console.log('  Password: ' + result.email);
    // console.log('  Password: ' + result.password);
    // console.log('  Password: ' + result.confirmPassword);
    if(result.password !== result.confirmPassword) { return onErr('Senhas não conferem'); }

    delete result.confirmPassword
    
    const newUser = {
        ...result,
        admin: true
    }

    newUser.password = encryptPassword(newUser.password)

    knex.from('users').where('email', '=', newUser.email).first()
    .then((user) => {
        if(!user){
            knex.from('users').insert(newUser).then(() => console.log("usuário criado"))
            .catch((err) => { console.log(err); throw err })
        } else {
            console.log("Usuário já existe, atualizando informações")
            newUser.deletedAt = null
            knex.from('users').update(newUser).where({id: user.id}).then(() => console.log("usuário atualizado"))
            .catch((err) => { console.log(err); throw err })
        }
    }).catch((err) => { console.log( err); throw err })
    .finally(() => {
        knex.destroy();
    });
});

function onErr(err) {
    console.log(err);
    return 1;
}