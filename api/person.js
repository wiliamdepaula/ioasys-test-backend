
module.exports = app => {

    const { existsOrError, notExistsOrError, equalsOrError  } = app.api.validation

    const save = async (req, res) => {
        const person = { ...req.body }

        try {
            existsOrError(person.name, "Nome não informado")

            const personFromDb = await app.db('persons')
                .where({ name: person.name })
                .first()

            if(!person.id) {
                notExistsOrError(personFromDb, 'Já existe alguém cadastrado com este nome')
            }

        } catch(msg) {
            return res.status(400).send(msg)
        }

        if(person.id) {
            app.db('persons')
                .update(person)
                .where( { id: person.id })
                .then(_ => res.status(204).send())
                .catch(err => res.status(500).send(err))
        } else {
            app.db('persons')
                .insert(person)
                .then(_ => res.status(204).send())
                .catch(err => res.status(500).send(err))
        }
    }

    const get = (req, res) => {
        app.db('persons')
            .then(results => res.json(results))
            .catch(err => res.status(500).send(err))
    }

    const getById = (req, res) => {
        app.db('persons')
            .where('persons.id', '=', req.params.id)
            .select(
                'persons.id','persons.name',
                app.db.raw('GROUP_CONCAT(DISTINCT directors.person ) as direct'),
                app.db.raw('GROUP_CONCAT(DISTINCT actors.film ) as acted')
            )
            .leftOuterJoin('directors', 'persons.id', '=', 'directors.person')
            .leftOuterJoin('actors', 'persons.id', '=', 'actors.person')
            .groupBy('persons.id')

            .then(results => res.json(results))
            .catch(err => res.status(500).send(err))
    }

    return { save, get, getById }
}