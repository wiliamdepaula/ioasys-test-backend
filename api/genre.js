module.exports = app => {
    const { existsOrError, notExistsOrError } = app.api.validation

    const save = async (req, res) => {
        const genre = { ...req.body }

        try {
            existsOrError(genre.name, "Informe o nome do gênero")

            const genreFromDb = await app.db('genres')
                .where({ name: genre.name})
                .first()

            if(!genre.id) {
                notExistsOrError(genreFromDb, 'Gênero já cadastrado')
            }
        } catch(msg) {
            return res.status(400).send(msg)
        }

        if(genre.id) {
            app.db('genres')
                .update(genre)
                .where({ id: genre.id })
                .then( _ => res.status(204).send())
                .catch(err => res.status(500).send(err))
        } else {
            app.db('genres')
                .insert(genre)  
                .then(_ => res.status(204).send())
                .catch(err => res.status(500).send(err))
        }
    }

    const get = (req, res) => {
        app.db('genres')
            .select('id', 'name')
            .then(genres => res.json(genres))
            .catch( err => res.status(500).send(err))
    }

    const getById = (req, res) => {
        app.db('genres')
            .select('id', 'name')
            .where({ id: req.params.id })
            .first()
            .then(genre => res.json(genre))
            .catch(err => res.status(500).send(err))
    }

    return { save, get, getById }
}