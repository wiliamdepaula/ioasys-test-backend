module.exports = app => {
    const get = (req, res) => {
        app.db('persons')
            .select('id', 'name', 'film as films')   
            .rightOuterJoin('actors', 'persons.id', '=', 'actors.person')
            .then(results => res.json(results))
            .catch(err => res.status(500).send(err))
    }

    const getById = (req, res) => {
        app.db('persons')
            .select('id', 'name', 'film as films')   
            .where({ id: req.params.id})
            .rightOuterJoin('actors', 'persons.id', '=', 'actors.person')
            .first()
            .then(person => res.json(person))
            .catch(err => res.status(500).send(err))
    }

    return { get, getById }
}