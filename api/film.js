module.exports = app => {
    const { existsOrError, notExistsOrError  } = app.api.validation

    const toArray = objectOriginal => {
        let newArray = []
        for(obj in objectOriginal) {
            newArray.push(objectOriginal[obj])
        }

        return newArray
    }

    const fabricFilmGenres = (film, genres) => genres.map( genre => ({
        "genre": genre,
        'film': film.id
    }))

    const saveGenres = async (res, genresFilm) => {
        await app.db('genres_films')
            .insert(genresFilm)
            .catch(err => res.status(500).send(err))
    }

    const delGenresByFilmId = async (filmId) => {
        await app.db('genres_films')
            .where( 'film', filmId)
            .del()
            .catch(err => res.status(500).send(err))
    }

    const fabricPerson = (film, persons) => persons.map( person => ({
        "person": person,
        'film': film.id
    }))

    const saveDirectors = async (res, filmDirectors) => {
        for( films in filmDirectors) {
            await app.db('directors')
                .insert(filmDirectors[films])
                .catch(err => res.status(500).send(err))
        }
    }

    const delDirectorsByFilmId = async (res, filmId) => {
            await app.db('directors')
            .where({film: filmId})
            .del()
            .catch(err => res.status(500).send(err))
    }

    const saveActor = async (res, filmActor) => {

        await app.db('actors')
            .insert(filmActor)
            .catch(err => res.status(500).send(err))
    }

    const delActorsByFilmId = async (res, filmId) => {
        await app.db('actors')
            .where({film: filmId})
            .del()
            .catch(err => res.status(500).send(err))
    }

    const fabricVote = (filmId, userID) => ({
        "person": userID,
        'film': filmId
    })
      

    const save = async (req, res) => {
        const film = { ...req.body }
        
        const genresObject = { ...film.genres }
        const genres = toArray(genresObject)
        delete film.genres

        const directorsObject = { ...film.directors }
        const directors = toArray(directorsObject)
        delete film.directors

        const actorsObject = { ...film.actors }
        const actors = toArray(actorsObject)
        delete film.actors

        try {
            existsOrError(film.name, "Nome não informado")
            existsOrError(film.year, "Ano de lançamento não informado")
            existsOrError(film.description, "Descrição não informada")
            existsOrError(directors, 'Diretor(es) não informado')
            existsOrError(actors, 'Ator(es) não informado')

            const filmFromDB = await app.db('films')
                .where({ name: film.name })
                .first()

            if(!film.id) {
                notExistsOrError(filmFromDB, 'Filme já cadastrado')
            }


            for(genreId in genres) {
                const genre = await app.db('genres')
                    .where({ id: genres[genreId] })
                    .first()    
                
                existsOrError(genre, "Gênero não cadastrado.")
            } 

            for(director in directors) {
                const direc = await app.db('persons')
                    .where({ id: directors[director] })
                    .first()    
                existsOrError(direc, "Diretor não cadastrado.")
            } 

            for(actor in actors) {
                const direc = await app.db('persons')
                    .where({ id: actors[actor] })
                    .first()    
                existsOrError(direc, "Ator não cadastrado.")
            } 
        } catch(msg) {
            return res.status(400).send(msg)
        }

        if(film.id) {
            const qtdFilmsUpdates = await app.db('films')
                .update(film)   
                .where({ id: film.id })
                .catch(err => res.status(500).send(err))

            await delGenresByFilmId(film.id)                               
            const genresFilm = fabricFilmGenres(film, genres)
            await saveGenres(res, genresFilm)

            const filmDirectors = fabricPerson(film, directors)
            await delDirectorsByFilmId(res, film.id)
            await saveDirectors(res, filmDirectors)

            const filmActors = fabricPerson(film, actors)
            await delActorsByFilmId(res, film.id)
            await saveActor(res, filmActors)

            res.status(204).send()
            
        } else {
            app.db('films')
                .insert(film)
                .then( async id => {
                    film.id = id[0]
                    const genresFilm = fabricFilmGenres(film, genres)
                    await saveGenres(res, genresFilm)

                    const filmDirectors = fabricPerson(film, directors)
                    await saveDirectors(res, filmDirectors)

                    const filmActors = fabricPerson(film, actors)
                    await saveActor(res, filmActors)


                    res.status(204).send()                 
                })
                .catch( err => res.status(500).send(err))
        }

    }

    const get = (req, res) => {
        app.db('films')
            .select(
                'films.id','films.name', 'year', 'films.imageUrl',
                app.db.raw('GROUP_CONCAT(DISTINCT genres_films.genre ) as genres'),
                app.db.raw('GROUP_CONCAT(DISTINCT directors.person ) as directors'),
                app.db.raw('GROUP_CONCAT(DISTINCT actors.person ) as actors')

            )
            .leftOuterJoin('genres_films', 'films.id', '=', 'genres_films.film')
            .leftOuterJoin('directors', 'films.id', '=', 'directors.film')
            .leftOuterJoin('actors', 'films.id', '=', 'actors.film')
            .groupBy('films.id')
            
            .then(results => res.json(results))
            .catch(err => res.status(500).send(err))
       
    }

    const getById = (req, res) => {
        
        app.db('films') 
            .where('films.id', '=',  req.params.id)
            .select(
                'films.id','films.name', 'year', 'films.imageUrl', 
                app.db.raw('GROUP_CONCAT(DISTINCT genres_films.genre ) as genres'),
                app.db.raw('GROUP_CONCAT(DISTINCT directors.person ) as directors'),
                app.db.raw('GROUP_CONCAT(DISTINCT actors.person ) as actors'),
                app.db.raw('AVG(votes.vote ) as starts'),

            )
            .leftOuterJoin('genres_films', 'films.id', '=', 'genres_films.film')
            .leftOuterJoin('directors', 'films.id', '=', 'directors.film')
            .leftOuterJoin('actors', 'films.id', '=', 'actors.film')
            .leftOuterJoin('votes', 'films.id', '=', 'votes.film')
            .groupBy('films.id')
            .first()            
            .then(results => res.json(results))
            .catch(err => res.status(500).send(err))
    }

    const vote = async (req, res) => {
        let body = req.body
        body.vote = parseInt(body.vote)
        
        try {
            const valorEntre0e4 = (body.vote>=0 && body.vote<=4)? true: false
            if ( body.vote !== 0) {
                existsOrError(body.vote, "Voto não informado")
            }
            existsOrError(valorEntre0e4, "Voto tem que ser maior ou igual a zero e menor ou igual a 4")
        } catch(msg) {
            return res.status(400).send(msg)
        }

        const user = { ...req.user }
        const vote = fabricVote(req.params.id, user.id)

        const voteFromDb = await app.db('votes')
            .where(vote)
            .first()
            .catch(err => res.status(500).send(err))

        vote.vote = body.vote

        if(voteFromDb) {
            app.db('votes')
                .update(vote)
                .then(results => res.json(results))
                .catch(err => res.status(500).send(err))
        } else {
            app.db('votes')
                .insert(vote)
                .then(results => res.status(200).send())
                .catch(err => res.status(500).send(err))
        }
    }

    return { save, get, getById, vote }
}