module.exports = app => {
    const get = (req, res) => {
        app.db('persons')
            .select('id', 'name', 'film as films')   
            .rightOuterJoin('directors', 'persons.id', '=', 'directors.person')
            .then(results => res.json(results))
            .catch(err => res.status(500).send(err))
    }

    const getById = (req, res) => {
        app.db('persons')   
            .select('id', 'name', 'film as films')   
            .where({ id: req.params.id})
            .rightOuterJoin('directors', 'persons.id', '=', 'directors.person')
            .first()
            .then(person => res.json(person))
            .catch(err => res.status(500).send(err))
    }

    return { get, getById }
}